# Intro to JavaScript Core Project Assignment Test Suites

This repository contains test suites to test the Intro to JS Core chapter project assignments.

### Steps 
1. Clone the repository to your local
2. Copy contents of `validator.js` and `utilities.js` from student's submitted assignments to `Validator.js` and `Utilities.js` respectively. **Make sure to copy only the contents and not the files directly as that will not pass the tests.**
3. Run `ValidatorSpecRunner.html` and `UtilitiesSpecRunner.html` in your browser window to see if all the test cases pass or not.
4. If any of the tests fail you will see red on the page and there will be stack traces. You can click on *Spec List* link as shown in the screenshot to look at which tests are failing and then look into `ValidatorSpec.js` OR `UtilitiesSpec.js` to go to the failed test and check the code. (I'll improve on this to make it easier to find what failed, later).

    ![](refimages/screenshot.PNG)
    
    After clicking on *Spec List*.

    ![](refimages/screenshot2.png)

5. Write up your report and send your feedback to the student
