describe('Validator', function () {
  var validator;

  beforeEach(function () {
    validator = window.validator || {};
  });

  // Check if Object itself exists or not
  it('object exists', function () {
    expect(validator).toBeDefined();
  })

  describe('.isEmailAddress', function () {
    it('function exists', function () {
      expect(validator.isEmailAddress).toBeDefined();
    })

    it('email string without @ symbol is invalid', function () {
      expect(validator.isEmailAddress('test')).toBe(false);
    })

    it('email string with @ symbol but without a domain is invalid', function () {
      expect(validator.isEmailAddress('test@')).toBe(false);
      expect(validator.isEmailAddress('test@xyz')).toBe(false);
      expect(validator.isEmailAddress('test@xyz.')).toBe(false);
    })

    it('email string with two or more @ symbol is invalid', function () {
      expect(validator.isEmailAddress('test@@')).toBe(false);
      expect(validator.isEmailAddress('test@@xyz.com')).toBe(false);
    })

    it('without email string without @ symbol but a proper domain is invalid', function () {
      expect(validator.isEmailAddress('xyz.com')).toBe(false);
    })

    it('without email string with @ symbol and a proper domain is invalid', function () {
      expect(validator.isEmailAddress('@xyz.com')).toBe(false);
    })

    it('email string without proper tld is invalid', function () {
      expect(validator.isEmailAddress('test@xyz..com')).toBe(false);
      expect(validator.isEmailAddress('test@xyz.co..eu')).toBe(false);
      expect(validator.isEmailAddress('test@xyz.co.in.')).toBe(false);
    })

    it('email string with @ symbol and a proper domain and tld is valid', function () {
      expect(validator.isEmailAddress('test@xyz.com')).toBe(true);
      expect(validator.isEmailAddress('test@xyz.co.in')).toBe(true);
      expect(validator.isEmailAddress('test@xyz.in')).toBe(true);
      expect(validator.isEmailAddress('test.email@xyz.in')).toBe(true);
    })
  })

  describe('.isPhoneNumber', function () {
    it('function exists', function () {
      expect(validator.isPhoneNumber).toBeDefined();
    })

    it('an invalid phone number string without any delimiters returns false', function () {
      expect(validator.isPhoneNumber('123456789')).toBe(false);
    })

    it('an invalid phone number string with delimiters returns false', function () {
      expect(validator.isPhoneNumber('123-456-789')).toBe(false);
      expect(validator.isPhoneNumber('123 456-789')).toBe(false);
      expect(validator.isPhoneNumber('123 456 789')).toBe(false);
      expect(validator.isPhoneNumber('123.456.789')).toBe(false);
      expect(validator.isPhoneNumber('(123) 456-789')).toBe(false);
    })

    it('a valid phone number without any delimiters returns true', function () {
      expect(validator.isPhoneNumber('0123456789')).toBe(true);
      expect(validator.isPhoneNumber('8888888888')).toBe(true);
    })

    it('a valid phone number with delimiters returns true', function () {
      expect(validator.isPhoneNumber('123-456-7890')).toBe(true);
      expect(validator.isPhoneNumber('123 456-7890')).toBe(true);
      expect(validator.isPhoneNumber('123 456 7890')).toBe(true);
      expect(validator.isPhoneNumber('123.456.7890')).toBe(true);
      expect(validator.isPhoneNumber('(123) 456-7890')).toBe(true);
    })

    it('a phone number with characters other than any delimiters is invalid', function () {
      expect(validator.isPhoneNumber('a123456789')).toBe(false);
      expect(validator.isPhoneNumber('8b8.888.8888')).toBe(false);
      expect(validator.isPhoneNumber('a1234567890')).toBe(false);
      expect(validator.isPhoneNumber('8b88.888.8888')).toBe(false);
    })
  })

  describe('.isDate', function () {
    it('function exists', function () {
      expect(validator.isDate).toBeDefined();
    })

    it('valid dates return true', function () {
      expect(validator.isDate('10-10-2016')).toBe(true);
      expect(validator.isDate('4-5-2012')).toBe(true);
      expect(validator.isDate('12-25-2016')).toBe(true);
    })

    it('invalid dates return false', function () {
      expect(validator.isDate('13-10-2016')).toBe(false);
      expect(validator.isDate('31-12-2012')).toBe(false);
      expect(validator.isDate('12252016')).toBe(false);
      expect(validator.isDate('abc')).toBe(false);
    })
  })

  describe('.isBeforeDate', function () {
    it('function exists', function () {
      expect(validator.isBeforeDate).toBeDefined();
    })
    var dec25, oct31;
    beforeEach(function () {
      dec25 = new Date('12-25-2016');
      oct31 = new Date('10-31-2016');
    })

    it('Valid Cases return true', function () {
      expect(validator.isBeforeDate('10-10-2016', '10-12-2016')).toBe(true);
      expect(validator.isBeforeDate(oct31, dec25)).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isBeforeDate('10-10-2016', '4-5-2012')).toBe(false);
      expect(validator.isBeforeDate(dec25, oct31)).toBe(false);
    })
  })

  describe('.isAfterDate', function () {
    it('function exists', function () {
      expect(validator.isAfterDate).toBeDefined();
    })
    var dec25, oct31;
    beforeEach(function () {
      dec25 = new Date('12-25-2016');
      oct31 = new Date('10-31-2016');
    })

    it('Valid cases return true', function () {
      expect(validator.isAfterDate('10-10-2016', '4-5-2012')).toBe(true);
      expect(validator.isAfterDate(dec25, oct31)).toBe(true);
    })

    it('Invalid Cases return false', function () {
      expect(validator.isAfterDate('10-10-2016', '10-12-2016')).toBe(false);
      expect(validator.isAfterDate(oct31, dec25)).toBe(false);
    })
  })

  describe('.isBeforeToday', function () {
    it('function exists', function () {
      expect(validator.isBeforeToday).toBeDefined();
    })
    var dec25, oct31;
    beforeEach(function () {
      dec25 = new Date('12-25-2017');
      oct31 = new Date('10-31-2016');
    })

    it('Valid Cases return true', function () {
      expect(validator.isBeforeToday('10-10-2016')).toBe(true);
      expect(validator.isBeforeToday(oct31)).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isBeforeToday('10-10-2017')).toBe(false);
      expect(validator.isBeforeToday(dec25)).toBe(false);
    })
  })

  describe('.isAfterToday', function () {
    it('function exists', function () {
      expect(validator.isAfterToday).toBeDefined();
    })
    var dec25, oct31;
    beforeEach(function () {
      dec25 = new Date('12-25-2017');
      oct31 = new Date('10-31-2016');
    })

    it('Valid Cases return true', function () {
      expect(validator.isAfterToday('10-10-2017')).toBe(true);
      expect(validator.isAfterToday(dec25)).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isAfterToday('10-10-2016')).toBe(false);
      expect(validator.isAfterToday(oct31)).toBe(false);
    })
  })

  describe('.isEmpty', function () {
    it('function exists', function () {
      expect(validator.isEmpty).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isEmpty('')).toBe(true);
      expect(validator.isEmpty(' ')).toBe(true);
      expect(validator.isEmpty('  ')).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isEmpty('Visiting new places is fun.')).toBe(false);
      expect(validator.isEmpty(null)).toBe(false);
      expect(validator.isEmpty(undefined)).toBe(false);
    })
  })

  describe('.isTrimmed', function () {
    it('function exists', function () {
      expect(validator.isTrimmed).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isTrimmed('harmony and irony')).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isTrimmed('   harmony and irony')).toBe(false);
      expect(validator.isTrimmed('harmony and irony      ')).toBe(false);
      expect(validator.isTrimmed('harmony  and  irony')).toBe(false);
    })
  })

  describe('.contains', function () {
    it('function exists', function () {
      expect(validator.contains).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.contains('Visiting new places is fun.', ['places'])).toBe(true);
      expect(validator.contains('Definitely, he said in a matter-of-fact tone.', ['matter', 'definitely'])).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.contains('Visiting new places is fun.', ['coconut'])).toBe(false);
      expect(validator.contains('Visiting new places is fun.', ['aces'])).toBe(false);
    })
  })

  describe('.lacks', function () {
    it('function exists', function () {
      expect(validator.lacks).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.lacks('Visiting new places is fun.', ['coconut'])).toBe(true);
      expect(validator.lacks('Visiting new places is fun.', ['aces'])).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.lacks('Visiting new places is fun.', ['places'])).toBe(false);
      expect(validator.lacks('Definitely, he said in a matter-of-fact tone.', ['matter', 'definitely'])).toBe(false);
    })
  })

  describe('.isComposedOf', function () {
    it('function exists', function () {
      expect(validator.isComposedOf).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isComposedOf('10184', ['1', '2', '3', '4', '5', '6' ,'7', '8', '9', '0'])).toBe(true);
      expect(validator.isComposedOf('I am ready.', ['I', 'I\'m', 'am', 'not', 'ready'])).toBe(true);
      expect(validator.isComposedOf('Iamnotready.', ['I', 'I\'m', 'am', 'not', 'ready'])).toBe(true);
      expect(validator.isComposedOf('applesound', ['apples', 'sound'])).toBe(true);
      expect(validator.isComposedOf('fooamazonFOO', ['Foo', 'Amazon'])).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isComposedOf('foobarbaz', ['foo', 'bar'])).toBe(false);
      expect(validator.isComposedOf('foobarbaz', ['foo', 'baz'])).toBe(false);
      expect(validator.isComposedOf('foobarbaz', ['foobar'])).toBe(false);
      expect(validator.isComposedOf('foobarbaz', ['foobaz', 'bar'])).toBe(false);
      expect(validator.isComposedOf('foobarbaz', ['fooba', 'bar'])).toBe(false);
      expect(validator.isComposedOf('foobarbaz', ['baz', 'foob'])).toBe(false);
      expect(validator.isComposedOf('applesound', ['sound', 'apple'])).toBe(true);
    })
  })

  describe('.isOfLengthOrLessThan', function () {
    it('function exists', function () {
      expect(validator.isOfLengthOrLessThan).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isOfLengthOrLessThan('123456789', 20)).toBe(true);
      expect(validator.isOfLengthOrLessThan('AHHHH', 25)).toBe(true);
      expect(validator.isOfLengthOrLessThan('This could be a tweet!', 140)).toBe(true);
      expect(validator.isOfLengthOrLessThan('test', 4)).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isOfLengthOrLessThan('123456789', 6)).toBe(false);
      expect(validator.isOfLengthOrLessThan('AHHHH', 2)).toBe(false);
      expect(validator.isOfLengthOrLessThan('This could be a tweet!', 10)).toBe(false);
      expect(validator.isOfLengthOrLessThan('test', 3)).toBe(false);
    })
  })

  describe('.isOfLengthOrGreaterThan', function () {
    it('function exists', function () {
      expect(validator.isOfLengthOrGreaterThan).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isOfLengthOrGreaterThan('123456789', 6)).toBe(true);
      expect(validator.isOfLengthOrGreaterThan('AHHHH', 2)).toBe(true);
      expect(validator.isOfLengthOrGreaterThan('This could be a tweet!', 10)).toBe(true);
      expect(validator.isOfLengthOrGreaterThan('test', 4)).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isOfLengthOrGreaterThan('123456789', 20)).toBe(false);
      expect(validator.isOfLengthOrGreaterThan('AHHHH', 25)).toBe(false);
      expect(validator.isOfLengthOrGreaterThan('This could be a tweet!', 140)).toBe(false);
      expect(validator.isOfLengthOrGreaterThan('test', 5)).toBe(false);
    })
  })

  describe('.lessWordsThan', function () {
    it('function exists', function () {
      expect(validator.lessWordsThan).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.lessWordsThan('I have four words', 6)).toBe(true);
      expect(validator.lessWordsThan('two words', 2)).toBe(true);
      expect(validator.lessWordsThan('This could be a tweet!', 10)).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.lessWordsThan('I have four words', 2)).toBe(false);
      expect(validator.lessWordsThan('two words', 1)).toBe(false);
      expect(validator.lessWordsThan('This could be a tweet!', 0)).toBe(false);
    })
  })

  describe('.moreWordsThan', function () {
    it('function exists', function () {
      expect(validator.moreWordsThan).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.moreWordsThan('I have four words', 2)).toBe(true);
      expect(validator.moreWordsThan('two words', 2)).toBe(true);
      expect(validator.moreWordsThan('This could be a tweet!', 0)).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.moreWordsThan('I have four words', 6)).toBe(false);
      expect(validator.moreWordsThan('two words', 3)).toBe(false);
      expect(validator.moreWordsThan('This could be a tweet!', 10)).toBe(false);
    })
  })

  describe('.isNumberBetween', function () {
    it('function exists', function () {
      expect(validator.isNumberBetween).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isNumberBetween(3, 2, 4)).toBe(true);
      expect(validator.isNumberBetween(1, 0, 10)).toBe(true);
      expect(validator.isNumberBetween(150, 0, 151)).toBe(true);
      expect(validator.isNumberBetween(2, 2, 2)).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isNumberBetween(3, 4, 5)).toBe(false);
      expect(validator.isNumberBetween(2, 3, 1)).toBe(false);
      expect(validator.isNumberBetween(11, 11, 10)).toBe(false);
    })
  })

  describe('.isAlphanumeric', function () {
    it('function exists', function () {
      expect(validator.isAlphanumeric).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isAlphanumeric('')).toBe(true);
      expect(validator.isAlphanumeric('supercalifragilisticexpialidocious')).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isAlphanumeric('Hello.')).toBe(false);
      expect(validator.isAlphanumeric('slam poetry')).toBe(false);
      expect(validator.isAlphanumeric('ArTᴉ$ʰARd')).toBe(false);

    })
  })

  describe('.isCreditCard', function () {
    it('function exists', function () {
      expect(validator.isCreditCard).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isCreditCard('1234-5678-9101-1121')).toBe(true);
      expect(validator.isCreditCard('1234567891011121')).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isCreditCard('----------------')).toBe(false);
      expect(validator.isCreditCard('testcard')).toBe(false);
    })
  })

  describe('.isHex', function () {
    it('function exists', function () {
      expect(validator.isHex).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isHex('#abcdef')).toBe(true);
      expect(validator.isHex('#bbb')).toBe(true);
      expect(validator.isHex('#1cf')).toBe(true);
      expect(validator.isHex('#1234a6')).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isHex('#bcdefg')).toBe(false);
      expect(validator.isHex('#1234a68')).toBe(false);
      expect(validator.isHex('cc4488')).toBe(false);
    })
  })

  describe('.isRGB', function () {
    it('function exists', function () {
      expect(validator.isRGB).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isRGB('rgb(0,0,0)')).toBe(true);
      expect(validator.isRGB('rgb(0, 0, 0)')).toBe(true);
      expect(validator.isRGB('rgb(255, 255, 112)')).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isRGB('rgba(0,0,0, 0)')).toBe(false);
      expect(validator.isRGB('rgb(0,300,0)')).toBe(false);
      expect(validator.isRGB('rgb(0,-14,0)')).toBe(false);
    })
  })

  describe('.isHSL', function () {
    it('function exists', function () {
      expect(validator.isHSL).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isHSL('hsl(0,0,0)')).toBe(true);
      expect(validator.isHSL('hsl(0, 0, 0)')).toBe(true);
      expect(validator.isHSL('hsl(255, 0, 1)')).toBe(true);
    })

    it('Invalid cases return false', function () {
      expect(validator.isHSL('hsl(0,0,0,0)')).toBe(false);
      expect(validator.isHSL('hsl(361,0,0)')).toBe(false);
      expect(validator.isHSL('hsl(0,-14,0)')).toBe(false);
      expect(validator.isHSL('hsl(0,0,11)')).toBe(false);
    })
  })

  describe('.isColor', function () {
    it('function exists', function () {
      expect(validator.isColor).toBeDefined();
    })

    it('Valid Cases return true', function () {
      expect(validator.isColor('#ccccff')).toBe(true);
      expect(validator.isColor('rgb(255,255,200)')).toBe(true);
      expect(validator.isColor('hsl(46,0.66,0.21)')).toBe(true);
      expect(validator.isColor('#363')).toBe(true);

    })

    it('Invalid cases return false', function () {
      expect(validator.isColor('hsl(255,255,255)')).toBe(false);
      expect(validator.isColor('abc345')).toBe(false);
      expect(validator.isColor('#3633')).toBe(false);
    })
  })
})
