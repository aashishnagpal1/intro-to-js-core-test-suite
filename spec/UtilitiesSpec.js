describe('Validator', function () {
  var utilities;

  beforeEach(function () {
    utilities = window.utilities || {};
  });

  // Check if Object itself exists or not
  it('object exists', function () {
    expect(utilities).toBeDefined();
  })

  describe('.by', function () {
    it('function exists', function () {
      expect(utilities.by).toBeDefined();
    })

    it('filter implementation works correctly', function () {
      var testArr = [1,2,3,4,5,6];
      var actual = [];
      var filter = function(val, index, list) {
        actual.push(val);
      };

      utilities.by(testArr, 2, filter);
      expect(actual).toEqual([2, 4, 6]);

      actual = [];
      utilities.by(testArr, 1, filter);
      expect(actual).toEqual(testArr);
    })

    it('multiply implementation works correctly', function () {
      var testArr = [1,2,3,4,5,6];
      var actual = [];
      var multiply = function(val, index, list) {
        actual.push(val*2);
      };

      utilities.by(testArr, 2, multiply);
      expect(actual).toEqual([4, 8, 12]);

      actual = [];
      utilities.by(testArr, 1, multiply);
      expect(actual).toEqual(testArr.map(function (val) {
        return val * 2;
      }));
    })

    it('square implementation works correctly', function () {
      var testArr = [1,2,3,4,5,6];
      var actual = [];
      var square = function(val, index, list) {
        actual.push(val*val);
      };

      utilities.by(testArr, 2, square);
      expect(actual).toEqual([4, 16, 36]);

      actual = [];
      utilities.by(testArr, 1, square);
      expect(actual).toEqual(testArr.map(function (val) {
        return val * val;
      }));
    })

  })

  describe('.keys', function () {
    it('function exists', function () {
      expect(utilities.keys).toBeDefined();
    })

    it('works correctly', function () {
      var object = {count: 5, length: 10, total: 16};
      expect(utilities.keys(object)).toEqual(['count', 'length', 'total']);

      object = {b: true, s: 'String', n: 12, a: [1,2,3]};
      expect(utilities.keys(object)).toEqual(['b', 's', 'n', 'a']);
    })

  })

  describe('.values', function () {
    it('function exists', function () {
      expect(utilities.values).toBeDefined();
    })

    it('works correctly', function () {
      var object = {count: 5, length: 10, total: 16};
      expect(utilities.values(object)).toEqual([5, 10, 16]);

      object = {b: true, s: 'String', n: 12, a: [1,2,3]};
      expect(utilities.values(object)).toEqual([true, 'String', 12, [1,2,3]]);
    })
  })

  describe('.pairs', function () {
    it('function exists', function () {
      expect(utilities.pairs).toBeDefined();
    })

    it('works correctly', function () {
      var object = {count: 5, length: 10, total: 16};
      expect(utilities.pairs(object)).toEqual(['count', 5, 'length', 10, 'total', 16]);

      object = {b: true, s: 'String', n: 12, a: [1,2,3]};
      expect(utilities.pairs(object)).toEqual(['b', true, 's', 'String', 'n', 12, 'a', [1,2,3]]);
    })
  })

  describe('.shuffle', function () {
    it('function exists', function () {
      expect(utilities.shuffle).toBeDefined();
    })

    it('works correctly', function () {
      var arr = [1,2,3,4,5];
      var arrOriginal = [1,2,3,4,5];
      expect(utilities.shuffle(arr)).not.toEqual(arrOriginal);
      expect(utilities.shuffle(arr)).not.toEqual(arrOriginal);
      expect(utilities.shuffle(arr)[0]).not.toEqual(arrOriginal[0]);
      expect(utilities.shuffle(arr)[1]).not.toEqual(arrOriginal[1]);
    })
  })

  describe('.pluralize', function () {
    it('function exists', function () {
      expect(utilities.pluralize).toBeDefined();
    })

    it('works correctly', function () {
      expect(utilities.pluralize(1, 'lion')).toBe('lion');
      expect(utilities.pluralize(2, 'lion')).toBe('lions');
      expect(utilities.pluralize(5, 'lion')).toBe('lions');
      expect(utilities.pluralize(0, 'lion')).toBe('lions');
      expect(utilities.pluralize(1, 'lioness')).toBe('lioness');
      expect(utilities.pluralize(2, 'lioness')).toBe('lionesss');
      expect(utilities.pluralize(2, 'lioness', 'lionesses')).toBe('lionesses');
    })
  })

  describe('.toDash', function () {
    it('function exists', function () {
      expect(utilities.toDash).toBeDefined();
    })

    it('works correctly', function () {
      expect(utilities.toDash('hotDog')).toBe('hot-dog');
      expect(utilities.toDash('spaceStationComplex')).toBe('space-station-complex');
      expect(utilities.toDash('myFirstFunction')).toBe('my-first-function');
      expect(utilities.toDash('testString')).toBe('test-string');
    })
  })

  describe('.toCamel', function () {
    it('function exists', function () {
      expect(utilities.toCamel).toBeDefined();
    })

    it('works correctly', function () {
      expect(utilities.toCamel('hot-dog')).toBe('hotDog');
      expect(utilities.toCamel('space-station-complex')).toBe('spaceStationComplex');
      expect(utilities.toCamel('my-first-function')).toBe('myFirstFunction');
      expect(utilities.toCamel('test-string')).toBe('testString');
    })
  })

  describe('.has', function () {
    it('function exists', function () {
      expect(utilities.has).toBeDefined();
    })

    it('works correctly', function () {
      expect(utilities.has({test: 'test1'}, 'test1')).toBe(true);
      expect(utilities.has({test: 'test1', test2: true}, true)).toBe(true);
    })
  })

  describe('.pick', function () {
    it('function exists', function () {
      expect(utilities.pick).toBeDefined();
    })

    it('works correctly', function () {
      var data = {
       type: 'transformer',
       index: 19,
       siblings: 19,
       access: 'full'
      };

      expect(utilities.pick(data, ['type', 'index'])).toEqual({type: 'transformer', index: 19});
      expect(utilities.pick(data, ['siblings', 'index'])).toEqual({siblings: 19, index: 19});
      expect(utilities.pick(data, ['access', 'animals'])).toEqual({access: 'full'});
      expect(utilities.pick(data, ['type', 'index'])).not.toEqual({});
      expect(utilities.pick(data, ['type', 'index'])).not.toEqual(data);
      expect(utilities.pick(data, [])).toEqual({});
      expect(utilities.pick(data, ['type', 'index', 'siblings', 'access'])).toEqual(data);
    })
  })

  describe('.withoutSymbols', function () {
    it('function exists', function () {
      expect(utilities.withoutSymbols).toBeDefined();
    })

    it('works correctly', function () {
      expect(utilities.withoutSymbols('Hi, john.doe@live.com., is that you?/')).toBe('Hi johndoelivecom is that you');
      expect(utilities.withoutSymbols('john.doe@live.com.')).toBe('johndoelivecom');
      expect(utilities.withoutSymbols('abc-test')).toBe('abctest');
      expect(utilities.withoutSymbols('`~!@#$%^&*()_-+=[{;:"\'/?.>,<}]')).toBe('');
    })
  })

  describe('.countWords', function () {
    it('function exists', function () {
      expect(utilities.countWords).toBeDefined();
    })

    it('works correctly', function () {
      expect(utilities.countWords('Hello.')).toBe(1);
      expect(utilities.countWords('Hard-to-type-really-fast!')).toBe(5);
      expect(utilities.countWords('')).toBe(0);
      expect(utilities.countWords('supercalifragilisticexpialidocious')).toBe(1);
      expect(utilities.countWords('A quick brown fox jumps over a lazy dog.')).toBe(9);
      expect(utilities.countWords('Definitely, he said in a matter-of-fact tone.')).toBe(9);
    })
  })

})
